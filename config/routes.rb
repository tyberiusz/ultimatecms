Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  scope '/admin' do
  	resources :pages, except: :show
  end
  
  root 'pages#index'

  get '/:slug', to: 'pages#show', as: :show
end
