class PagesController < ApplicationController

  def index
    @pages = Page.all
  end

  def new
    @page = Page.new
  end

  def show
    @page = Page.where(slug: params[:slug]).take
  end

  def create
    @page = Page.new(page_params)
 
    if @page.save
      redirect_to :action => 'index'
    else
      render 'new'
    end
  end

  private
  def page_params
    params.require(:page).permit(:title, :description, :slug, :follow, :index)
  end

end
