class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :title
      t.text :description
      t.string :slug
      t.boolean :index
      t.boolean :follow

      t.timestamps
    end
  end
end
